
#include <stdio.h>
#include <stdlib.h>

#include <curl/curl.h>

#include "config.h"
#include "videofile.h"


int main(int argc, char** argv)
{
    if(argc != 2)
    {
        printf("USAGE: %s <video URL>\n", argv[0]);
        return EXIT_FAILURE;
    }

    curl_global_init(CURL_GLOBAL_ALL);

    const char* URL = argv[1];

    download_videofile(get_config_url(URL));

    curl_global_cleanup();

    return EXIT_SUCCESS;
}
