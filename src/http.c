
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <curl/curl.h>

static const char* useragent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36";

struct buffer_t {
  char* memory;
  size_t size;
};

static size_t
request_callback(void* contents, size_t size, size_t nmemb, void* userp)
{
  size_t realsize = size * nmemb;
  struct buffer_t* buffer = (struct buffer_t*)userp;

  char* ptr = realloc(buffer->memory, buffer->size + realsize + 1);
  if(!ptr)
  {
      perror("realloc");
      return 0;
  }

  buffer->memory = ptr;
  memcpy(&buffer->memory[buffer->size], contents, realsize);
  buffer->size += realsize;
  buffer->memory[buffer->size] = 0;
  return realsize;
}

char* do_request(const char* URL)
{
    CURL* curl = curl_easy_init();
    if(!curl)
    {
        perror("curl_easy_init");
        return NULL;
    }

    struct buffer_t buffer = {NULL, 0};

    curl_easy_setopt(curl, CURLOPT_URL, URL);
    curl_easy_setopt(curl, CURLOPT_USERAGENT, useragent);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, request_callback);

    if(curl_easy_perform(curl) != CURLE_OK)
    {
        perror("curl_easy_perform");
        curl_easy_cleanup(curl);
        return NULL;
    }

    curl_easy_cleanup(curl);
    return buffer.memory;
}

int do_download(const char* URL, const char* filename)
{
    CURL* curl = curl_easy_init();
    if(!curl)
        return 1;

    FILE* file = fopen(filename, "wb");

    curl_easy_setopt(curl, CURLOPT_URL, URL);
    curl_easy_setopt(curl, CURLOPT_USERAGENT, useragent);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, file);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, fwrite);

    if(curl_easy_perform(curl) != CURLE_OK)
    {
        fclose(file);
        return 2;
    }

    curl_easy_cleanup(curl);
    fclose(file);

    return 0;
}
