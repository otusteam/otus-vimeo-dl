
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parson.h"

#include "http.h"


static const char* config_marker = "window.vimeo.clip_page_config.player = ";

char* get_config_url(const char* video_URL)
{
    char* page = do_request(video_URL);
    char* config_start = strstr(page, config_marker);
    if(!config_start)
    {
        fprintf(stderr, "config not found\n");
        free(page);
        return NULL;
    }

    size_t i = 0;
    for(; config_start[i] != ';' && config_start[i] != 0; i++);
    config_start[i] = 0;

    for(; *config_start != '{' && *config_start != 0; config_start++);

    JSON_Value* config_json = json_parse_string((char*)config_start);
    if(json_value_get_type(config_json) != JSONObject)
    {
        fprintf(stderr, "get_config_url: config is not an object\n");
        json_value_free(config_json);
        free(page);
        return NULL;
    }

    JSON_Object* config_object = json_value_get_object(config_json);
    size_t len = json_object_get_string_len(config_object, "config_url");
    if(!len)
    {
        fprintf(stderr, "get_config_url: no config_url found\n");
        json_value_free(config_json);
        free(page);
        return NULL;
    }
    char* result = (char*)malloc(len);
    strncpy(result, json_object_get_string(config_object, "config_url"), len + 1);

    json_value_free(config_json);
    free(page);

    return result;
}
