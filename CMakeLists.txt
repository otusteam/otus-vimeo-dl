cmake_minimum_required(VERSION 3.5)
project(otus-vimeo-dl C)

set(CMAKE_C_STANDARD 11)

add_subdirectory("contrib/parson")

find_package(CURL REQUIRED)

include_directories(${CURL_INCLUDE_DIRS} "contrib/parson")

set(SOURCES
  "src/main.c"
  "src/http.c"
  "src/config.c"
  "src/videofile.c"
  )
set(HEADERS
  "src/http.h"
  "src/config.h"
  "src/videofile.h"
  )

add_executable(otus-vimeo-dl ${SOURCES} ${HEADERS})
target_link_libraries(otus-vimeo-dl ${CURL_LIBRARIES} parson)
